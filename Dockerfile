FROM openjdk:8-jre
LABEL MAINTAINER="Alexander Gorlov <alexander.gorlov@t-systems.com>"
ENTRYPOINT ["java", "-jar", "/usr/local/eventqueue/eventqueue-0.0.1.jar"]
ARG JAR_FILE
ADD target/${JAR_FILE} /usr/local/eventqueue/eventqueue-0.0.1.jar