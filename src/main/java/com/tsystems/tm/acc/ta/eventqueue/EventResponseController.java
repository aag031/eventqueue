package com.tsystems.tm.acc.ta.eventqueue;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaTypeFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "/", produces = "application/json")
public class EventResponseController {
    private static final int CODE_SUCCESS = 200;
    private static final int CODE_QUEUE_EMPTY = 204;
    private final EventQueueHandler eventQueueHandler;

    public EventResponseController() {
        this.eventQueueHandler = new EventQueueHandler();
    }

    @GetMapping(value = "/cioeventqueue", produces = "application/json")
    public ResponseEntity<CioEventObject> getFirstCioEvent() {
        final Optional<CioEventObject> event = this.eventQueueHandler.getCioEvent();
        if (event.isPresent()) {
            return new ResponseEntity<CioEventObject>(event.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<CioEventObject>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/foleventqueue", produces = "application/json")
    public ResponseEntity<FolEventObject> getFirstFolEvent() {
        final Optional<FolEventObject> event = this.eventQueueHandler.getFolEvent();
        if (event.isPresent()) {
            return new ResponseEntity<FolEventObject>(event.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<FolEventObject>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/cioeventqueue/size", produces = "application/json")
    public ResponseEntity<Integer> getCioEventQueueSize() {
        final int queueSize = this.eventQueueHandler.cioEventQueueSize();
        return new ResponseEntity<Integer>(queueSize, HttpStatus.OK);
    }

    @GetMapping(value = "/foleventqueue/size", produces = "application/json")
    public ResponseEntity<Integer> getFolEventQueueSize() {
        final int queueSize = this.eventQueueHandler.folEventQueueSize();
        return new ResponseEntity<Integer>(queueSize, HttpStatus.OK);
    }

    @PostMapping(value = "/cioeventqueue", produces = "application/json")
    public ResponseEntity<CioEventObject> pushCioEvent(@RequestBody CioEventObject request) {
        this.eventQueueHandler.pushCioEvent(request);
        return new ResponseEntity<CioEventObject>(HttpStatus.OK);
    }

    @PostMapping("/foleventqueue")
    public ResponseEntity<FolEventObject> pushFolEvent(@RequestBody FolEventObject request) {
        this.eventQueueHandler.pushFolEvent(request);
        return new ResponseEntity<FolEventObject>(HttpStatus.OK);
    }
}
