package com.tsystems.tm.acc.ta.eventqueue;

import java.util.ArrayDeque;
import java.util.Optional;
import java.util.Queue;

public class EventQueueHandler {
    public static final String FOL_EVENT_QUEUE_NAME = "foleventqueue";
    public static final String CIO_EVENT_QUEUE_NAME = "cioeventqueue";
    private final Queue<CioEventObject> cioEventQueue;
    private final Queue<FolEventObject> folEventQueue;

    public EventQueueHandler() {
        this.cioEventQueue = new ArrayDeque<>();
        this.folEventQueue = new ArrayDeque<>();
    }

    public void pushCioEvent(CioEventObject event) {
        this.cioEventQueue.add(event);
    }

    public void pushFolEvent(FolEventObject event) {
        this.folEventQueue.add(event);
    }

    public Optional<CioEventObject> getCioEvent() {
        if (this.cioEventQueue.size() > 0 ) {
            return Optional.of(this.cioEventQueue.poll());
        } else {
            return Optional.empty();
        }
    }

    public Optional<FolEventObject> getFolEvent() {
        if (this.folEventQueue.size() > 0 ) {
            return Optional.of(this.folEventQueue.poll());
        } else {
            return Optional.empty();
        }
    }

    public int cioEventQueueSize() {
        return this.cioEventQueue.size();
    }

    public int folEventQueueSize() {
        return this.cioEventQueue.size();
    }
}
