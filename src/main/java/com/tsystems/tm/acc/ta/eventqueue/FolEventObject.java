package com.tsystems.tm.acc.ta.eventqueue;

import lombok.*;

import java.time.OffsetDateTime;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class FolEventObject {
    private String eventId = null;
    private String eventSource = null;
    private String eventStream = null;
    private OffsetDateTime eventDateTime = null;
    private String eventType = null;
    private List<String> eventMetaData = null;
    private String eventVersion = null;
    private Object eventPayload = null;
}
